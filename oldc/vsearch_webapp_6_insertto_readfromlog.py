


from flask import Flask, render_template, request, escape

from listprimes import primenumbersupto

from DBcm import UseDatabase


app = Flask(__name__)

'''
--------------------------------------------------------------------------------------------------------------
-------------------Functions to increase Readability----------------------------------------------------------
--------------------------------------------------------------------------------------------------------------

'''

def readlog_split_listofstrings_separatedbysep_to_listoflists() -> 'list_of_lists':
  contents = [] #this will be a list of lists
  with open('vsearch.log') as log:
      for line in log:
          contents.append([]) #appending an empty list to our contents list
          for item in line.split('|'): #each 'item' in line is separated by |
              contents[-1].append(escape(item)) #populating the last item(list) in our list
  return contents  


'''
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------

'''

def log_request(req: 'flask_request', res: str) -> None:
    """Log details of the web request and the results."""
    with open('vsearch.log', 'a') as log:
        print(req.form, req.remote_addr, req.user_agent, res, file=log, sep='|') #str(dir(req))



@app.route('/entry')
def entry_page() -> 'html':
    """Returns the entry page to browser."""
    return render_template('entry.html',
                           the_title='Welcome to search for primes on the web!')

@app.route('/search4', methods=['POST'])
def search4() -> 'html':
    """Returns the results of a call to 'search4letters' to the browser."""
    target = request.form['target']
    results = str(primenumbersupto(target))
    log_request(request, results)
    return render_template('results.html',
                           the_title='Here are your results!',
                           the_target=target,
                           the_results=results)

@app.route('/viewlog')
def view_the_log() -> 'html':
  """Display the contents of the log file as a HTML table."""
  contents = readlog_split_listofstrings_separatedbysep_to_listoflists()
  titles = ('Form Data', 'Remote_addr', 'User_agent', 'Results')
  return render_template('viewlog.html',
                         the_title='View Log',
                         the_row_titles=titles,
                         the_data=contents,)


if __name__ == '__main__':
    app.run(debug=True)


