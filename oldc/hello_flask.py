from flask import Flask, render_template
from listprimes import primenumbersupto

app = Flask(__name__)

@app.route('/')
def hello() -> str:
	return 'Hello World from Flask!'


@app.route('/listprimes')

def do_prime_listing() -> str:
	return str(primenumbersupto(100))


@app.route('/entry')
def entry_page() -> 'html':
	return render_template('entry.html', 
		the_title='Welcome to Prime Number Searcher')



app.run()