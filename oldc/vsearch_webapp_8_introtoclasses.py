
class CountFromBy:

  def __init__(self, v: int=0, i: int=1) -> None:
      self.val = v
      self.incr = i

  def increase(self, increase) -> None:
      self.val = self.val+(self.incr*increase)

  def __repr__(self) -> str:
      return str(self.val)